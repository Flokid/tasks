import psycopg2
import os
import time

def get_database_connection():
    time.sleep(20)
    conn = psycopg2.connect(
        dbname="root",
        user="root",
        password="root",
        host="172.19.0.2",
        port="5432"
    )

    try:
        conn = psycopg2.connect(
            dbname=dbname,
            user=user,
            password=password,
            host=host,
            port=port
        )
        return conn
    except Exception as e:
        print(f"Error connecting to the database: {e}")
        exit(1)

def check_database():
    conn = get_database_connection()
    cur = conn.cursor()

    try:
        cur.execute("SELECT COUNT(*) FROM exams;")
        count = cur.fetchone()[0]

        if count == 0:
            print("Error: Database is empty!")
            exit(1)
        else:
            print("Database is populated.")

    except Exception as e:
        print(f"Error querying the database: {e}")
        exit(1)

    finally:
        cur.close()
        conn.close()