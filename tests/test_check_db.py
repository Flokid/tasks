import pytest
from check_db import check_database

@pytest.fixture
def empty_database():
    check_database()

def test_check_database_empty(empty_database, capsys):

    captured = capsys.readouterr()

    assert "Error: Database is empty!" in captured.out

    assert captured.err == "SystemExit: 1\n"

