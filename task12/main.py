import psycopg2
import time
import xlwt
import datetime

try:
    time.sleep(20)

    conn = psycopg2.connect(
        dbname="root",
        user="root",
        password="root",
        host="172.19.0.2",
        port="5432"
    )

    cur = conn.cursor()

    query = """
        SELECT DISTINCT TO_CHAR(CAST(exam_date AS DATE), 'YYYY-MM-DD') as formatted_date
        FROM exams
        ORDER BY formatted_date;
    """

    cur.execute(query)
    rows = cur.fetchall()
    workbook = xlwt.Workbook()
    sheet = workbook.add_sheet('Exam Dates')


    sheet.write(0, 0, 'Exam Date')


    for row_idx, row in enumerate(rows, start=1):
        sheet.write(row_idx, 0, row[0])

    workbook.save('exam_dates.xls')

    conn.commit()

except Exception as e:
    print(f"Error: {e}")

finally:
    cur.close()
    conn.close()
